package com.gu.xiang.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gu.xiang.dto.ResultDO;
import com.gu.xiang.entity.TOrder;

/**
 * IOrderService
 *
 * @author Gu
 * @date 2023/2/11
 * @Description:
 */
public interface ITOrderService extends IService<TOrder> {
    ResultDO pageList(int currentPage, int pageSize, Long orderNumber);

    ResultDO queryDetail(Long orderId);
}
