package com.gu.xiang.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gu.xiang.dto.ResultDO;
import com.gu.xiang.entity.Voucher;

import java.math.BigDecimal;
import java.util.List;

/**
 * IVoucherService
 *
 * @author Gu
 * @date 2023/2/10
 * @Description:
 */
public interface IVoucherService extends IService<Voucher> {
    ResultDO pageList(int currentPage, int pageSize, BigDecimal amount);

    ResultDO addVoucher(Voucher voucher);

    ResultDO isLaunch(List<Long> ids, Boolean isLaunch);

    ResultDO changeData(Voucher voucher);

    ResultDO removeVoucher(List<Long> ids);
}
