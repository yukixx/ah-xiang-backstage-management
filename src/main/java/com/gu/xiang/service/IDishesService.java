package com.gu.xiang.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gu.xiang.dto.DishesDTO;
import com.gu.xiang.dto.ResultDO;
import com.gu.xiang.entity.Dishes;
import com.gu.xiang.entity.Employee;

import java.util.List;

/**
 * IDishesService
 *
 * @author Gu
 * @date 2023/2/9
 * @Description:
 */
public interface IDishesService extends IService<Dishes> {

    ResultDO pageList(int currentPage, int pageSize, String name);

    ResultDO isLaunch(List<Long> ids, Boolean isLaunch);

    ResultDO removeDishes(List<Long> ids);

    ResultDO changeData(Dishes dishes);

    ResultDO addDishes(Dishes dishes);
}
