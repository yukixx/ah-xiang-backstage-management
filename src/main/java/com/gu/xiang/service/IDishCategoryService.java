package com.gu.xiang.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gu.xiang.entity.DishCategory;
import com.gu.xiang.mapper.DishCategoryMapper;

/**
 * IDishCategoryService
 *
 * @author Gu
 * @date 2023/2/9
 * @Description:
 */
public interface IDishCategoryService extends IService<DishCategory> {
}
