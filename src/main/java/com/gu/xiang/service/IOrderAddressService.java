package com.gu.xiang.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gu.xiang.entity.OrderAddress;

/**
 * IOrderAddressService
 *
 * @author Gu
 * @date 2023/2/11
 * @Description:
 */
public interface IOrderAddressService extends IService<OrderAddress> {
}
