package com.gu.xiang.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gu.xiang.dto.EmployeeDTO;
import com.gu.xiang.dto.LoginFormDTO;
import com.gu.xiang.dto.PhoneLoginDTO;
import com.gu.xiang.dto.ResultDO;
import com.gu.xiang.entity.Employee;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * IEmployeeService
 *
 * @author Gu
 * @date 2023/2/2
 * @Description:
 */
public interface IEmployeeService extends IService<Employee> {
    ResultDO loginDO(LoginFormDTO loginForm, HttpSession session);
    
    
    ResultDO pageList(int currentPage, int pageSize, String name);

    ResultDO isStart(List<Long> ids, Boolean isStart);

    ResultDO changeData(EmployeeDTO employeeDTO);

    ResultDO removeEmployee(List<Long> ids);

    ResultDO addEmployee(Employee employee);

    ResultDO sendCode(String phone);

    ResultDO codeLoginDo(PhoneLoginDTO loginForm, HttpSession session);
}
