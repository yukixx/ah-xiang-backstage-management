package com.gu.xiang.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gu.xiang.entity.Store;

/**
 * IStoreService
 *
 * @author Gu
 * @date 2023/2/16
 * @Description:
 */
public interface IStoreService extends IService<Store> {
}
