package com.gu.xiang.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gu.xiang.dto.OrderDTO;
import com.gu.xiang.dto.ResultDO;
import com.gu.xiang.dto.VoucherDTO;
import com.gu.xiang.entity.Dishes;
import com.gu.xiang.entity.OrderAddress;
import com.gu.xiang.entity.TOrder;
import com.gu.xiang.entity.Voucher;
import com.gu.xiang.mapper.TOrderMapper;
import com.gu.xiang.service.IDishesService;
import com.gu.xiang.service.IOrderAddressService;
import com.gu.xiang.service.ITOrderService;
import com.gu.xiang.service.IVoucherService;
import com.gu.xiang.utils.EmployeeHolder;
import com.gu.xiang.utils.PageUtil;
import com.gu.xiang.utils.VoucherCategoryEnum;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * OrderServiceImpl
 *
 * @author Gu
 * @date 2023/2/11
 * @Description:
 */
@Service
public class TOrderServiceImpl extends ServiceImpl<TOrderMapper, TOrder> implements ITOrderService {
    @Resource
    private ITOrderService orderService;
    @Resource
    private IOrderAddressService orderAddressService;
    @Resource
    private IDishesService dishesService;
    @Resource
    private IVoucherService voucherService;
    @Override
    public ResultDO pageList(int currentPage, int pageSize, Long orderNumber) {
        String storeAccount = EmployeeHolder.getEmployee().getStoreAccount();

        //2.查看是否模糊查询
        if( orderNumber != null){
            //2.1如果订单号不为空，进行模糊分页查询
            Page<TOrder> page = orderService.query().like("id", orderNumber)
                    .eq("store_account",storeAccount)//商家号应该是从employeeHolder中获取,这里是开发测试
                    .page(new Page<>(currentPage, pageSize));
            //2.2获取当前页数据
            List<OrderDTO> collect = PageUtil.getCurrentPage(page, OrderDTO.class);
//            //2.3设置其他要返回的数据
//            for (OrderDTO orderDTO : collect){
//                //根据枚举Id查询类型名字
//                Integer categoryId = orderDTO.getCategoryId();
//                if(categoryId != null){
//                    String categoryValue = VoucherCategoryEnum.getCategoryValue(categoryId);
//                    if(StrUtil.isBlank(categoryValue)){
//                        orderDTO.setCategoryName(categoryValue);
//
//                    }
//                }
//                //设置对应菜品
//                Long dishesId = voucherDTO.getDishesId();
//                if(dishesId != null ){
//                    Dishes byId = dishesService.getById(dishesId);
//                    if(byId!=null){
//                        voucherDTO.setDishesName(byId.getName());
//                    }
//                }
//
//            }
            //2.4返回当前页数据以及数据总量
            return ResultDO.ok(collect, page.getTotal());
        }

        //3.如果不是，分页查询所有代金券列表
        Page<TOrder> page = orderService.query().eq("store_account",storeAccount).orderByDesc("create_time")  //这里同样
                .page(new Page<>(currentPage, pageSize));

        //4.获取当前页数据
        List<OrderDTO> collect = page.getRecords().stream().map(item -> {
            OrderDTO orderDTO = BeanUtil.copyProperties(item, OrderDTO.class);
            orderDTO.setUserName("小谷");//这里应该查数据库，这里是测试
            //查询地址信息
            OrderAddress address = orderAddressService.getById(item.getAddressId());
            orderDTO.setAddress(address);
            return orderDTO;
        }).collect(Collectors.toList());
//        //5.设置其他要返回的数据
//        for (VoucherDTO voucherDTO : collect){
//            //根据枚举Id查询类型名字
//            Integer categoryId = voucherDTO.getCategoryId();
//            if(categoryId != null){
//                String categoryValue = VoucherCategoryEnum.getCategoryValue(categoryId);
//                if(!StrUtil.isBlank(categoryValue)){
//                    voucherDTO.setCategoryName(categoryValue);
//                }
//            }
//            //设置对应菜品
//            Long dishesId = voucherDTO.getDishesId();
//            if(dishesId != null ){
//                Dishes byId = dishesService.getById(dishesId);
//                if(byId!=null){
//                    voucherDTO.setDishesName(byId.getName());
//                }
//            }
//        }
        //6.返回当前页数据以及数据总量
        return ResultDO.ok(collect, page.getTotal());
    }

    @Override
    public ResultDO queryDetail(Long orderId) {
        //1.判断订单类型
        TOrder order = orderService.getById(orderId);
        if(order == null){
            return ResultDO.fail("订单不存在");
        }
        Integer orderCategory = order.getOrderCategory();
        if(orderCategory == 1){
            //是菜品订单,查询菜品
            Long goodsId = order.getGoodsId();
            Dishes dishes = dishesService.getById(goodsId);
            if(dishes == null){
                return ResultDO.fail("订的菜品不存在");
            }
            return ResultDO.ok("所购买的是菜品：" + dishes.getName());

        }else{
            //是代金券订单
            Long goodsId = order.getGoodsId();
            Voucher voucher = voucherService.getById(goodsId);
            if(voucher == null){
                return ResultDO.fail("订的菜品不存在");
            }
            return ResultDO.ok("所购买的是代金券："+voucher.getName());
        }
    }
}
