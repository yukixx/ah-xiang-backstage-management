package com.gu.xiang.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gu.xiang.entity.Store;
import com.gu.xiang.mapper.StoreMapper;
import com.gu.xiang.service.IStoreService;
import org.springframework.stereotype.Service;

/**
 * StoreServiceImpl
 *
 * @author Gu
 * @date 2023/2/16
 * @Description:
 */
@Service
public class StoreServiceImpl extends ServiceImpl<StoreMapper, Store> implements IStoreService {
}
