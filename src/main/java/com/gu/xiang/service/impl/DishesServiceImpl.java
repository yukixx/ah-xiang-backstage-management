package com.gu.xiang.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gu.xiang.dto.DishesDTO;
import com.gu.xiang.dto.EmployeeDTO;
import com.gu.xiang.dto.ResultDO;
import com.gu.xiang.entity.DishCategory;
import com.gu.xiang.entity.Dishes;
import com.gu.xiang.entity.Employee;
import com.gu.xiang.mapper.DishesMapper;
import com.gu.xiang.service.IDishCategoryService;
import com.gu.xiang.service.IDishesService;
import com.gu.xiang.utils.DishCategoryEnum;
import com.gu.xiang.utils.EmployeeHolder;
import com.gu.xiang.utils.PageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * DishesServiceImpl
 *
 * @author Gu
 * @date 2023/2/9
 * @Description:
 */
@Service
@Slf4j
public class DishesServiceImpl extends ServiceImpl<DishesMapper, Dishes> implements IDishesService{
    @Resource
    private IDishesService dishesService;
    @Resource
    private IDishCategoryService dishCategoryService;

    //分页查询菜品
    @Override
    public ResultDO pageList(int currentPage, int pageSize, String name) {

        //1.获取当前登录用户所在商铺号
        EmployeeDTO employee = EmployeeHolder.getEmployee();
        String storeAccount = employee.getStoreAccount();

        //2.查看是否模糊查询
        if(!StrUtil.isBlank(name)){
            //2.1如果name不为空，进行模糊分页查询
            Page<Dishes> page = dishesService.query().like("name", name)
                    .eq("store_account",storeAccount)//商家号应该是从employeeHolder中获取,这里是开发过程
                    .page(new Page<>(currentPage, pageSize));
            //2.2获取当前页数据
            List<DishesDTO> collect = PageUtil.getCurrentPage(page, DishesDTO.class);
            //2.3将菜品类型名字添加上
            for (DishesDTO dishesDTO : collect){
                Long categoryId = dishesDTO.getCategoryId();
                if(categoryId != null){
                    dishesDTO.setCategoryName(DishCategoryEnum.getCategoryValue(categoryId));
                }else{
                    continue;
                }
            }
            //2.4返回当前页数据以及数据总量
            return ResultDO.ok(collect, page.getTotal());
        }

        //3.如果不是，分页查询所有菜品列表
        Page<Dishes> page = dishesService.query().eq("store_account",storeAccount)  //这里同样
                .page(new Page<>(currentPage, pageSize));

        //4.获取当前页数据
        List<DishesDTO> collect = PageUtil.getCurrentPage(page, DishesDTO.class);
        //5.将菜品类型名加上
        for (DishesDTO dishesDTO : collect){
            Long categoryId = dishesDTO.getCategoryId();
            if(categoryId != null){
                dishesDTO.setCategoryName(DishCategoryEnum.getCategoryValue(categoryId));
            }else{
                continue;
            }
        }
        LinkedList<Object> result = new LinkedList<>();
        //6.返回当前页数据以及数据总量
        return ResultDO.ok(collect, page.getTotal());
    }

    //更改菜品状态（包含批量）
    @Override
    @Transactional
    public ResultDO isLaunch(List<Long> ids, Boolean isLaunch) {
        Long id;
        boolean isSuccess;
        Arrays.asList();
        //1.判断是批量更改还是单改
        if(ids.size() == 1){
            id = ids.get(0);
            //判断id是否合规...
            //单独更改菜品状态
            isSuccess = dishesService.update().setSql("status = " + isLaunch).eq("id", id).update();
        }else{
            //批量更改菜品状态
            isSuccess = dishesService.update().setSql("status = " + isLaunch).in("id", ids).update();
        }
        //2.判断是否更改成功
        if(!isSuccess){
            if(isLaunch){
                ResultDO.fail("起售失败");
            }else {
                ResultDO.fail("停售失败");
            }
        }
        //3.返回结果
        return ResultDO.ok();
    }

    //删除菜品（包含批量）
    @Transactional
    @Override
    public ResultDO removeDishes(List<Long> ids) {
        Long id;
        boolean isSuccess;
        //1.判断是单独删除还是批量删除
        if(ids.size() == 1){
            id = ids.get(0);
            //单独删除
            isSuccess = dishesService.removeById(id);
        }else{

            //LinkedList<Long> idList = new LinkedList<>();
            //Collections.addAll(idList, ids);  将数组转换为list 最高效方法
            isSuccess = dishesService.removeByIds(ids);
        }
        //2.判断是否删除成功
        if(!isSuccess){
            return ResultDO.fail("删除失败");
        }
        //3.返回结果
        return ResultDO.ok();
    }

    //改变菜品信息
    @Override
    public ResultDO changeData(Dishes dishes) {

        //1.更改菜品信息
        boolean isSuccess = dishesService.updateById(dishes);
        //2.判断是否更改成功
        if(!isSuccess){
            return ResultDO.fail("更改失败");
        }
        //3.返回结果
        return ResultDO.ok();
    }

    //添加菜品
    @Override
    public ResultDO addDishes(Dishes dishes) {
        //1.设置其他数据
        //1.1设置创建菜品时间
        dishes.setCreateTime(LocalDateTime.now());
        EmployeeDTO employeeDTO = EmployeeHolder.getEmployee();

        //1.2设置创建菜品的用户,以及商铺号
        if(employeeDTO != null){
            Long id = employeeDTO.getId();
            String storeAccount = employeeDTO.getStoreAccount();
            dishes.setCreateUser(id);
            dishes.setStoreAccount(storeAccount);
        }else{  //按正常来说不可能为空，如果为空应返回错误信息，这里只是开发测试
            dishes.setCreateUser(1L);
            dishes.setStoreAccount("haochi");
        }
        //1.3设置菜品所在商铺号
        EmployeeDTO employee = EmployeeHolder.getEmployee();
        String storeAccount = employee.getStoreAccount();
        dishes.setStoreAccount(storeAccount);
        //2.添加菜品
        boolean isSuccess = dishesService.save(dishes);
        //3.判断是否成功
        if(isSuccess == false){
            return ResultDO.fail("添加失败");
        }
        //4.返回结果
        return ResultDO.ok();
    }
}
