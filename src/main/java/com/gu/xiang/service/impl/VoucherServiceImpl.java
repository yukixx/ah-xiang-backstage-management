package com.gu.xiang.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gu.xiang.dto.DishesDTO;
import com.gu.xiang.dto.EmployeeDTO;
import com.gu.xiang.dto.ResultDO;
import com.gu.xiang.dto.VoucherDTO;
import com.gu.xiang.entity.Dishes;
import com.gu.xiang.entity.Voucher;
import com.gu.xiang.mapper.VoucherMapper;
import com.gu.xiang.service.IDishesService;
import com.gu.xiang.service.IVoucherService;
import com.gu.xiang.utils.EmployeeHolder;
import com.gu.xiang.utils.PageUtil;
import com.gu.xiang.utils.VoucherCategoryEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * VoucherServiceImpl
 *
 * @author Gu
 * @date 2023/2/10
 * @Description:
 */
@Service
@Slf4j
public class VoucherServiceImpl extends ServiceImpl<VoucherMapper, Voucher> implements IVoucherService {
    @Resource
    private IVoucherService voucherService;
    @Resource
    private IDishesService dishesService;
    //分页查询
    @Override
    public ResultDO pageList(int currentPage, int pageSize, BigDecimal amount) {
        //1.获取当前登录用户所在商铺号
        EmployeeDTO employee = EmployeeHolder.getEmployee();
        String storeAccount = employee.getStoreAccount();
        //2.查看是否模糊查询
        if( amount != null){
            //2.1如果金额不为空，进行模糊分页查询
            Page<Voucher> page = voucherService.query().like("amount", amount)
                    .eq("store_account",storeAccount)//商家号应该是从employeeHolder中获取,这里是开发测试
                    .page(new Page<>(currentPage, pageSize));
            //2.2获取当前页数据
            List<VoucherDTO> collect = PageUtil.getCurrentPage(page, VoucherDTO.class);
            //2.3设置其他要返回的数据
            for (VoucherDTO voucherDTO : collect){
                //根据枚举Id查询类型名字
                Integer categoryId = voucherDTO.getCategoryId();
                if(categoryId != null){
                    String categoryValue = VoucherCategoryEnum.getCategoryValue(categoryId);
                    if(StrUtil.isBlank(categoryValue)){
                        voucherDTO.setCategoryName(categoryValue);
                        
                    }
                }
                //设置对应菜品
                Long dishesId = voucherDTO.getDishesId();
                if(dishesId != null ){
                    Dishes byId = dishesService.getById(dishesId);
                    if(byId!=null){
                        voucherDTO.setDishesName(byId.getName());
                    }
                }

            }
            //2.4返回当前页数据以及数据总量
            return ResultDO.ok(collect, page.getTotal());
        }

        //3.如果不是，分页查询所有代金券列表
        Page<Voucher> page = voucherService.query().eq("store_account",storeAccount).orderByDesc("amount")  //这里同样
                .page(new Page<>(currentPage, pageSize));

        //4.获取当前页数据
        List<VoucherDTO> collect = PageUtil.getCurrentPage(page, VoucherDTO.class);
        //5.设置其他要返回的数据
        for (VoucherDTO voucherDTO : collect){
            //根据枚举Id查询类型名字
            Integer categoryId = voucherDTO.getCategoryId();
            if(categoryId != null){
                String categoryValue = VoucherCategoryEnum.getCategoryValue(categoryId);
                if(!StrUtil.isBlank(categoryValue)){
                    voucherDTO.setCategoryName(categoryValue);
                }
            }
            //设置对应菜品
            Long dishesId = voucherDTO.getDishesId();
            if(dishesId != null ){
                Dishes byId = dishesService.getById(dishesId);
                if(byId!=null){
                    voucherDTO.setDishesName(byId.getName());
                }
            }
        }
        //6.返回当前页数据以及数据总量
        return ResultDO.ok(collect, page.getTotal());
    }

    @Override
    public ResultDO addVoucher(Voucher voucher) {
        //1.设置其他数据
        //1.1设置创建代金券的时间
        voucher.setCreateTime(LocalDateTime.now());
        EmployeeDTO employeeDTO = EmployeeHolder.getEmployee();

        //1.2设置创建代金券的用户,以及商铺号
        if(employeeDTO != null){
            Long id = employeeDTO.getId();
            String storeAccount = employeeDTO.getStoreAccount();
            voucher.setCreateUser(id);
            voucher.setStoreAccount(storeAccount);
        }else{  //按正常来说不可能为空，如果为空应返回错误信息，这里只是开发测试
            return ResultDO.fail("登录信息错误，请重新登录");
        }
        //2.添加代金券
        boolean isSuccess = voucherService.save(voucher);
        //3.判断是否成功
        if(isSuccess == false){
            return ResultDO.fail("添加失败");
        }
        //4.返回结果
        return ResultDO.ok();
    }

    //更改代金券的状态（包含批量更改）
    @Transactional
    @Override
    public ResultDO isLaunch(List<Long> ids, Boolean isLaunch) {
        Long id;
        boolean isSuccess;
        //1.判断是批量更改还是单改
        if(ids.size() == 1){
            id = ids.get(0);
            //判断id是否合规...
            //单独更改代金券状态
            isSuccess = voucherService.update().setSql("status = " + isLaunch).eq("id", id).update();
        }else{
            //批量更改代金券状态
            isSuccess = voucherService.update().setSql("status = " + isLaunch).in("id", ids).update();
        }
        //2.判断是否更改成功
        if(!isSuccess){
            if(isLaunch){
                ResultDO.fail("起售失败");
            }else {
                ResultDO.fail("停售失败");
            }
        }
        //3.返回结果
        return ResultDO.ok();
    }

    //改变代金券信息
    @Override
    public ResultDO changeData(Voucher voucher) {
        //1.判断代金券类型
        Integer categoryId = voucher.getCategoryId();
        System.out.println(categoryId);
        if(categoryId != null && categoryId == 1){
            voucher.setDishesId(null);
            log.info("哈哈");
        }
        //1.更改代金券信息
        boolean isSuccess = voucherService.updateById(voucher);
        //2.判断是否更改成功
        if(!isSuccess){
            return ResultDO.fail("更改失败");
        }
        //3.返回结果
        return ResultDO.ok();
    }

    //删除代金券(包含批量)
    @Transactional
    @Override
    public ResultDO removeVoucher(List<Long> ids) {
        Long id;
        boolean isSuccess;
        //1.判断是单独删除还是批量删除
        if(ids.size() == 1){
            id = ids.get(0);
            //单独删除
            isSuccess = voucherService.removeById(id);
        }else{

            //LinkedList<Long> idList = new LinkedList<>();
            //Collections.addAll(idList, ids);  将数组转换为list 最高效方法
            isSuccess = voucherService.removeByIds(ids);
        }
        //2.判断是否删除成功
        if(!isSuccess){
            return ResultDO.fail("删除失败");
        }
        //3.返回结果
        return ResultDO.ok();
    }
}
