package com.gu.xiang.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.copier.Copier;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gu.xiang.dto.EmployeeDTO;
import com.gu.xiang.dto.LoginFormDTO;
import com.gu.xiang.dto.PhoneLoginDTO;
import com.gu.xiang.dto.ResultDO;
import com.gu.xiang.entity.Employee;
import com.gu.xiang.mapper.EmployeeMapper;
import com.gu.xiang.service.IEmployeeService;
import com.gu.xiang.utils.EmployeeHolder;
import com.gu.xiang.utils.PageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static com.gu.xiang.utils.RedisConstants.LOGIN_EMPLOYEE_KEY;
import static com.gu.xiang.utils.RedisConstants.LOGIN_EMPLOYEE_TTL;

/**
 * EmployeeServiceImpl
 *
 * @author Gu
 * @date 2023/2/2
 * @Description:
 */
@Service
@Slf4j
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements IEmployeeService {
    @Resource
    private IEmployeeService employeeService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private EmployeeMapper employeeMapper;
    //登录业务
    @Override
    public ResultDO loginDO(LoginFormDTO loginForm, HttpSession session) {
        //1.校验员工账号和商家号
        String account = loginForm.getAccount();
        String storeAccount = loginForm.getStoreAccount();
        //1.1判断账号是否为空
        if(StrUtil.isBlank(account) || StrUtil.isBlank(storeAccount)){
            //如果为空返回错误原因
            return ResultDO.fail("请填写账号或密码");
        }
        //1.2查看此商家的员工账号是否存在
        Employee employee = employeeService.query().eq("account", account).eq("store_account", storeAccount).one();
        if(employee == null){
            //如果不存在返回错误原因
            return ResultDO.fail("请填写正确的账号与密码");
        }
        //2.校验密码
        String password = loginForm.getPassword();
        //2.1判断是否为空
        if(StrUtil.isBlank(password)){
            //如果为空返回错误原因
            return ResultDO.fail("请填写账号或密码");
        }
        //2.2查看密码是否正确
        String md5 = SecureUtil.md5(password);
        log.info(md5);
        log.info(employee.getPassword());
        if(!md5.equals(employee.getPassword())){
            //如果不正确返回错误原因
            return ResultDO.fail("请填写正确账号或密码");
        }

        //3.如果存在，将用户信息信息保存redis中(生成一个登录令牌)
        String token = UUID.randomUUID().toString();
        EmployeeDTO employeeDTO = BeanUtil.copyProperties(employee, EmployeeDTO.class);
        //3.1将employeeDTO转为HashMap存储
        Map<String, String> employeeMsg = new HashMap<>();
        employeeMsg.put("id", String.valueOf(employeeDTO.getId()));
        employeeMsg.put("name", employeeDTO.getName());
        employeeMsg.put("avatar", employeeDTO.getAvatar());
        employeeMsg.put("permission", String.valueOf(employeeDTO.getPermission()));
        employeeMsg.put("storeAccount", employeeDTO.getStoreAccount());
        //3.2存到redis
        stringRedisTemplate.opsForHash().putAll(LOGIN_EMPLOYEE_KEY + token, employeeMsg);
        //设置token有效期
        stringRedisTemplate.expire(LOGIN_EMPLOYEE_KEY + token, LOGIN_EMPLOYEE_TTL, TimeUnit.MINUTES);
        //4.返回token
        return ResultDO.ok(token);
    }
    @Override
    public ResultDO codeLoginDo(PhoneLoginDTO loginForm, HttpSession session) {
        //1.获取手机号验证码
        String phone = loginForm.getPhone();
        String code = loginForm.getCode();
        //2.查询验证码是否存在
        String cacheCode = stringRedisTemplate.opsForValue().get("login:code:" + phone);
        if (cacheCode == null || !cacheCode.equals(code)) {
            // 不一致，报错
            return ResultDO.fail("验证码错误");
        }
        //3.如果存在，将用户信息信息保存redis中(生成一个登录令牌)
        Employee employee = employeeService.query().eq("phone", phone).one();
        String token = UUID.randomUUID().toString();
        EmployeeDTO employeeDTO = BeanUtil.copyProperties(employee, EmployeeDTO.class);
        //3.1将employeeDTO转为HashMap存储
        Map<String, String> employeeMsg = new HashMap<>();
        employeeMsg.put("id", String.valueOf(employeeDTO.getId()));
        employeeMsg.put("name", employeeDTO.getName());
        employeeMsg.put("avatar", employeeDTO.getAvatar());
        employeeMsg.put("permission", String.valueOf(employeeDTO.getPermission()));
        employeeMsg.put("storeAccount", employeeDTO.getStoreAccount());
        //3.2存到redis
        stringRedisTemplate.opsForHash().putAll(LOGIN_EMPLOYEE_KEY + token, employeeMsg);
        //设置token有效期
        stringRedisTemplate.expire(LOGIN_EMPLOYEE_KEY + token, LOGIN_EMPLOYEE_TTL, TimeUnit.MINUTES);
        //4.返回token
        return ResultDO.ok(token);
    }
    //分页查询业务
    @Override
    public ResultDO pageList(int currentPage, int pageSize, String name) {
        //1.获取当前登录用户所在商铺号
        EmployeeDTO employee = EmployeeHolder.getEmployee();
        String storeAccount = employee.getStoreAccount();
        //2.查看是否模糊查询
        if(!StrUtil.isBlank(name)){
            //2.1如果name不为空，进行模糊分页查询
            Page<Employee> page = employeeService.query().like("name", name)
                    .eq("store_account",storeAccount)//商家号应该是从employeeHolder中获取,这里是开发过程
                    .page(new Page<>(currentPage, pageSize));
            //2.2获取当前页数据
            List<EmployeeDTO> collect = PageUtil.getCurrentPage(page, EmployeeDTO.class);
//                    page.getRecords().stream()
//                    //.filter(employee-> !employee.getAccount().equals("admin"))
//                    .map(employee -> BeanUtil.copyProperties(employee, EmployeeDTO.class))
//                    .collect(Collectors.toList());
            //2.3返回当前页数据以及数据总量
            return ResultDO.ok(collect, page.getTotal());
        }

        //3.如果不是，分页查询所有员工列表
        Page<Employee> page = employeeService.query().eq("store_account",storeAccount)  //这里同样
                .page(new Page<>(currentPage, pageSize));

        //4.获取当前页数据
        List<EmployeeDTO> collect = PageUtil.getCurrentPage(page,EmployeeDTO.class);
//                page.getRecords().stream()
//                //.filter(employee-> !employee.getAccount().equals("admin"))
//                .map(employee -> BeanUtil.copyProperties(employee, EmployeeDTO.class))
//                .collect(Collectors.toList());
        //5.返回当前页数据以及数据总量
        return ResultDO.ok(collect, page.getTotal());
    }
    //启用禁用业务
    @Transactional
    @Override
    public ResultDO isStart(List<Long> ids, Boolean isStart) {
        Long id;
        boolean isSuccess;

        //1.判断是批量更改还是单改
        if(ids.size() == 1){
            id = ids.get(0);
            //判断id是否合规...
            //单独更改员工状态
            isSuccess = employeeService.update().setSql("status = " + isStart).eq("id", id).update();
        }else{
            //批量更改员工状态
            isSuccess = employeeService.update().setSql("status = " + isStart).in("id", ids).update();
        }
        //2.判断是否更改成功
        if(!isSuccess){
            if(isStart){
                ResultDO.fail("启用失败");
            }else {
                ResultDO.fail("禁用失败");
            }
        }
        //3.返回结果
        return ResultDO.ok();
    }
    //更改员工信息业务
    @Override
    public ResultDO changeData(EmployeeDTO employeeDTO) {
        Employee employee = BeanUtil.copyProperties(employeeDTO, Employee.class);

        //1.更改员工信息
        boolean isSuccess = employeeService.updateById(employee);
        //2.判断是否更改成功
        if(!isSuccess){
            return ResultDO.fail("更改失败");
        }
        //3.返回结果
        return ResultDO.ok();
    }
    //删除员工（包含批量）
    @Transactional
    @Override
    public ResultDO removeEmployee(List<Long> ids) {
        Long id;
        boolean isSuccess;
        //1.判断是单独删除还是批量删除
        if(ids.size() == 1){
            id = ids.get(0);
            //单独删除
            isSuccess = employeeService.removeById(id);
        }else{

            //LinkedList<Long> idList = new LinkedList<>();
            //Collections.addAll(idList, ids);  将数组转换为list 最高效方法
            isSuccess = employeeService.removeByIds(ids);
        }
        //2.判断是否删除成功
        if(!isSuccess){
            return ResultDO.fail("删除失败");
        }

        //3.返回结果
        return ResultDO.ok();
    }
    //添加员工
    @Override
    public ResultDO addEmployee(Employee employee) {
        EmployeeDTO employeeDTO = EmployeeHolder.getEmployee();
        String storeAccount = employeeDTO.getStoreAccount();
        //设置商铺号
        employee.setStoreAccount(storeAccount);
        //查询此商铺账号是否已经存在
        Employee eq = employeeService.query().eq("account", employee.getAccount()).eq("store_account", storeAccount).one();
        if(eq != null){
            return ResultDO.fail("此账号已经存在");
        }
        //1.设置其他数据
        //1.1设置添加员工时间
        employee.setCreateTime(LocalDateTime.now());
        //1.2设置添加员工的用户
        Long id = employeeDTO.getId();
        employee.setCreateUser(id);

        log.info(storeAccount);
        //2.添加员工
        //将员工密码加密
        String md5 = SecureUtil.md5(employee.getPassword());
        employee.setPassword(md5);
        boolean isSuccess = employeeService.save(employee);
        //3.判断是否成功
        if(isSuccess == false){
            return ResultDO.fail("添加失败");
        }
        //4.返回结果
        return ResultDO.ok();
    }

    @Override
    public ResultDO sendCode(String phone) {
        //1.验证手机号是否存在
        Employee employee = employeeService.query().eq("phone", phone).one();
        if(employee == null){
            return ResultDO.fail("你的手机号并未注册");
        }
        //2.存在，生成验证码
        String code = RandomUtil.randomNumbers(5);
        //3.将验证码存到redis中
        stringRedisTemplate.opsForValue().set("login:code:" + phone, code, 1L, TimeUnit.MINUTES);
        //4.发送验证码
        //5.返回
        return ResultDO.ok();
    }

}
