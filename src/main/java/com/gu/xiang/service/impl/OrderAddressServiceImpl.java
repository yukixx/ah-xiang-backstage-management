package com.gu.xiang.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gu.xiang.entity.OrderAddress;
import com.gu.xiang.mapper.OrderAddressMapper;
import com.gu.xiang.service.IOrderAddressService;
import org.springframework.stereotype.Service;

/**
 * OrderAddressServiceImpl
 *
 * @author Gu
 * @date 2023/2/11
 * @Description:
 */
@Service
public class OrderAddressServiceImpl extends ServiceImpl<OrderAddressMapper, OrderAddress> implements IOrderAddressService {
}
