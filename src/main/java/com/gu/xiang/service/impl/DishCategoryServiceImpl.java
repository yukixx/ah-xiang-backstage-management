package com.gu.xiang.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gu.xiang.entity.DishCategory;
import com.gu.xiang.mapper.DishCategoryMapper;
import com.gu.xiang.service.IDishCategoryService;
import org.springframework.stereotype.Service;

/**
 * DishCategoryServiceImpl
 *
 * @author Gu
 * @date 2023/2/9
 * @Description:
 */
@Service
public class DishCategoryServiceImpl extends ServiceImpl<DishCategoryMapper, DishCategory> implements IDishCategoryService {
}
