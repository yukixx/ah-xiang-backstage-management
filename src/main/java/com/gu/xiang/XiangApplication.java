package com.gu.xiang;

import org.apache.poi.ss.usermodel.Workbook;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
@SpringBootApplication
@EnableTransactionManagement
@MapperScan("com.gu.xiang.mapper")
public class XiangApplication {
    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(XiangApplication.class);
        springApplication.setBannerMode(Banner.Mode.OFF);
        springApplication.run(args);
        SpringApplication.run(XiangApplication.class,args);
    }
}
