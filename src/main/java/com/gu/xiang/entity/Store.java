package com.gu.xiang.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * Store
 *
 * @author Gu
 * @date 2023/2/9
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = false)
//这个注释会 使使用 setter方法返回所使用的对象
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@TableName("store")
public class Store {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String name;
    private String account;
    private Long addressId;
    private String phone;
    private LocalDateTime createTime;
}
