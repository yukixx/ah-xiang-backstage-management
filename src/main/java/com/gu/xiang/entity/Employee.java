package com.gu.xiang.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Employee
 *
 * @author Gu
 * @date 2023/2/2
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = false)
//这个注释会 使使用 setter方法返回所使用的对象
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@TableName("employee")
public class Employee implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 员工姓名
     */
    private String name;

    /**
     * 用户名
     */
    private String account;

    /**
     * 密码
     */
    private String password;

    /**
     * 员工头像
     */
    private String avatar;
    /**
     * 权限
     */
    private Boolean permission;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 性别
     */
    private Boolean sex;

    /**
     * 身份证号
     */
    private String idNumber;

    /**
     * 商店号
     */
    private String storeAccount;

    /**
     * 状态
     */
    private Boolean status;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 更新人
     */
    private Long updateUser;
}
