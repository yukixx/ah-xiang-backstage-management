package com.gu.xiang.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * DishCategory
 *
 * @author Gu
 * @date 2023/2/9
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = false)
//这个注释会 使使用 setter方法返回所使用的对象
@Accessors(chain = true)
@TableName("dish_category")
@AllArgsConstructor
@NoArgsConstructor
public class DishCategory {
    private Long id;
    private String name;
    private LocalDateTime createTime;
    private Long createUser;
    private Long updateUser;
}
