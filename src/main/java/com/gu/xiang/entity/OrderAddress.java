package com.gu.xiang.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * OrderAddress
 *
 * @author Gu
 * @date 2023/2/11
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = false)
//这个注释会 使使用 setter方法返回所使用的对象
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@TableName("order_address")
public class OrderAddress {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private Long userId;
    private String consignee;
    private String phone;
    private String province;
    private String city;
    private String district;
    private String detail;
    private Boolean isDefault;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;

}
