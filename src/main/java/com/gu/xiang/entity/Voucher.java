package com.gu.xiang.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Voucher
 *
 * @author Gu
 * @date 2023/2/10
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = false)
//这个注释会 使使用 setter方法返回所使用的对象
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@TableName("voucher")
public class Voucher {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 对应菜品Id（可以无）
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Long dishesId;

    /**
     * 代金券名字
     */
    private String name;

    /**
     * 代金券金额
     */
    private BigDecimal amount;

    /**
     * 代金券类型（使用枚举Id，1代表通用，2代表限制菜品）
     */
    private Integer categoryId;

    /**
     * 代金券售价
     */
    private BigDecimal price;

    /**
     * 对应的商铺号
     */
    private String storeAccount;

    /**
     * 状态
     */
    private Boolean status;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 更新人
     */
    private Long updateUser;
}
