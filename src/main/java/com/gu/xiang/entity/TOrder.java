package com.gu.xiang.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Order
 *
 * @author Gu
 * @date 2023/2/11
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = false)
//这个注释会 使使用 setter方法返回所使用的对象
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_order")
public class TOrder {
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    private Long userId;
    private Long goodsId;
    private Integer number;
    private String phone;
    private Integer orderCategory;  //1菜品，2代金券
    private Integer status; //订单状态
    private Integer payType;    //支付方式
    private BigDecimal trueAmount;  //实收金额
    private Long addressId;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;


}
