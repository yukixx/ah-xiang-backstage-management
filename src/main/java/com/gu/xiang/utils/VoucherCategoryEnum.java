package com.gu.xiang.utils;

import java.util.Arrays;

/**
 * VoucherCategoryEnum
 *
 * @author Gu
 * @date 2023/2/10
 * @Description:
 */
public enum VoucherCategoryEnum {
    GENERAL(1, "通用"), LIMIT(2, "限制菜品");
    private Integer id;
    private String categoryName;
    private VoucherCategoryEnum(){
    }
    private VoucherCategoryEnum(Integer id, String categoryName){
        this.id = id;
        this.categoryName = categoryName;
    }
    public Integer getId(){
        return id;
    }
    public String getCategoryName(){
        return categoryName;
    }
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    //根据枚举id（类型id查询枚举内容）
    public static String getCategoryValue(Integer id){
        VoucherCategoryEnum[] categoryEnums = values();
        for (VoucherCategoryEnum categoryEnum : categoryEnums) {
            if (categoryEnum.id == id) {
                return categoryEnum.getCategoryName();
            }
        }
        return null;
    }
    //根据枚举内容查询枚举Id
    public static Integer getId(String categoryName) {
        VoucherCategoryEnum[] categoryEnums = VoucherCategoryEnum.values();
        for (VoucherCategoryEnum categoryEnum : categoryEnums) {
            if (categoryEnum.getCategoryName().equals(categoryName)) {
                return categoryEnum.getId();
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "VoucherCategoryEnum{" +
                "id=" + id +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}
