package com.gu.xiang.utils;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * PageUtil
 *
 * @author Gu
 * @date 2023/2/9
 * @Description:
 */
public class PageUtil {
    //获取当前页集合
    public static  <R> List<R> getCurrentPage(Page<R> page){
        if(page == null){
            return null;
        }
        List<R> collect = page.getRecords().stream().collect(Collectors.toList());
        return collect;
    }
    //获取当前页集合并转换类型
    public static  <R, T> List<T> getCurrentPage(Page<R> page, Class<T> type){
        if(page == null){
            return null;
        }
        List<T> currentList = page.getRecords().stream().map(item -> BeanUtil.copyProperties(item, type)).collect(Collectors.toList());
        return currentList;

    }

}
