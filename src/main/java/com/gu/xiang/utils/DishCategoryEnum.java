package com.gu.xiang.utils;

/**
 * DishCategoryEnum
 *
 * @author Gu
 * @date 2023/2/10
 * @Description:
 */
public enum DishCategoryEnum {
    HOTDISH(1L, "热菜"), COLDDISH(2L, "凉菜"),SNACK(3L, "小吃"),
    STAPLEFOOD(4L, "主食");

    private Long id;
    private String categoryName;
    private DishCategoryEnum(){
    }
    private DishCategoryEnum(Long id, String categoryName){
        this.id = id;
        this.categoryName = categoryName;
    }
    public Long getId(){
        return id;
    }
    public String getCategoryName(){
        return categoryName;
    }
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    public void setId(Long id) {
        this.id = id;
    }
    //根据枚举id（类型id查询枚举内容）
    public static String getCategoryValue(Long id){
        DishCategoryEnum[] categoryEnums = values();
        for (DishCategoryEnum categoryEnum : categoryEnums) {
            if (categoryEnum.id == id) {
                return categoryEnum.getCategoryName();
            }
        }
        return null;
    }
    //根据枚举内容查询枚举Id
    public static Long getId(String categoryName) {
        DishCategoryEnum[] categoryEnums = values();
        for (DishCategoryEnum categoryEnum : categoryEnums) {
            if (categoryEnum.getCategoryName().equals(categoryName)) {
                return categoryEnum.getId();
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "VoucherCategoryEnum{" +
                "id=" + id +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}
