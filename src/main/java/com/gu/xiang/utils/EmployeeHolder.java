package com.gu.xiang.utils;

import com.gu.xiang.dto.EmployeeDTO;

/**
 * UserHolder
 *
 * @author Gu
 * @date 2023/2/2
 * @Description:    当前登录用户信息类
 */
public class EmployeeHolder {
    private static final ThreadLocal<EmployeeDTO> tl = new ThreadLocal();
    //将当前登录员工信息保存
    public static void saveEmployee(EmployeeDTO employeeDTO){
        tl.set(employeeDTO);
    }
    //获得当前登录员工信息
    public static EmployeeDTO getEmployee(){
        return tl.get();
    }
    //清除当前登录员工信息
    public static void remove(){
        tl.remove();
    }

}
