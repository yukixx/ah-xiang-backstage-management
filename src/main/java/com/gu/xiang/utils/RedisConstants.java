package com.gu.xiang.utils;

/**
 * RedisConstants
 *
 * @author Gu
 * @date 2023/2/12
 * @Description:
 */
public class RedisConstants {
    public static final String LOGIN_EMPLOYEE_KEY = "login:employee:token:";
    public static final String LOGIN_USER_KEY = "login:user:token:";
    public static final String CACHE_DISHES_KEY="cache:dishes:";
    public static final long LOGIN_EMPLOYEE_TTL =60L;
}
