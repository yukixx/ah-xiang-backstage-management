package com.gu.xiang.controller;

import cn.hutool.core.bean.BeanUtil;
import com.gu.xiang.dto.ResultDO;
import com.gu.xiang.dto.StatusDTO;
import com.gu.xiang.entity.Dishes;
import com.gu.xiang.entity.Voucher;
import com.gu.xiang.service.IVoucherService;
import com.gu.xiang.utils.EmployeeHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * VoucherController
 *
 * @author Gu
 * @date 2023/2/10
 * @Description:
 */
@RestController
@RequestMapping("voucher")
public class VoucherController {
    @Resource
    private IVoucherService voucherService;
    //分页查询（带金额模糊）
    @GetMapping(value={"/{currentPage}/{pageSize}/{amount}", "/{currentPage}/{pageSize}"})
    public ResultDO listPage(@PathVariable(value = "currentPage", required = true) int currentPage,
                             @PathVariable(value = "pageSize", required = true) int pageSize,
                             @PathVariable(value = "amount", required = false) BigDecimal amount){
        //1.判空
        if (currentPage <=0 || pageSize <= 0){
            return ResultDO.fail("错误");
        }
        //2.查询
        ResultDO resultDO = voucherService.pageList(currentPage, pageSize, amount);
        //3.返回
        return resultDO;
    }
    //添加代金券
    @PostMapping("/add")
    public ResultDO addVoucher(@RequestBody Voucher voucher){
        //判断账号有没有权限
        if(!EmployeeHolder.getEmployee().getPermission()){
            return ResultDO.fail("抱歉，你的账号没有此权限");
        }
        //1.判空
        if(voucher == null){
            return ResultDO.fail("出错啦");
        }
        //2.业务处理
        ResultDO resultDO = voucherService.addVoucher(voucher);
        //3.返回结果
        return resultDO;
    }
    //起售停售代金券
    @PutMapping("/isLaunch")
    public ResultDO isLaunch(@RequestBody StatusDTO statusDTO){
        //1.判空
        if(statusDTO == null){
            return ResultDO.fail("错误");
        }
        if(statusDTO.getIds() == null){
            return ResultDO.fail("错误");
        }
        //2.处理业务
        ResultDO resultDO = voucherService.isLaunch(statusDTO.getIds(), statusDTO.getIsStart());
        //3.返回结果
        return resultDO;
    }

    //修改代金券信息
    @PutMapping("/changeVoucher")
    public ResultDO change(@RequestBody Voucher voucher){
        //判断账号有没有权限
        if(!EmployeeHolder.getEmployee().getPermission()){
            return ResultDO.fail("抱歉，你的账号没有此权限");
        }
        //1.判空
        if(BeanUtil.isEmpty(voucher)){
            return ResultDO.fail("出错了");
        }
        //2.处理业务
        ResultDO resultDO = voucherService.changeData(voucher);
        //3.返回结果
        return resultDO;
    }

    //删除代金券
    @DeleteMapping("/remove")
    public ResultDO remove(@RequestBody List<Long> ids){
        //判断账号有没有权限
        if(!EmployeeHolder.getEmployee().getPermission()){
            return ResultDO.fail("抱歉，你的账号没有此权限");
        }
        //1.判空
        if(ids == null){
            return ResultDO.fail("出错了");
        }
        //2.处理业务
        ResultDO resultDO = voucherService.removeVoucher(ids);
        //3.返回结果
        return resultDO;
    }
}
