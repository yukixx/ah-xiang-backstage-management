package com.gu.xiang.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.gu.xiang.dto.*;
import com.gu.xiang.entity.Employee;
import com.gu.xiang.entity.Store;
import com.gu.xiang.service.IEmployeeService;
import com.gu.xiang.service.IStoreService;
import com.gu.xiang.utils.EmployeeHolder;
import com.gu.xiang.utils.SystemConstants;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.gu.xiang.utils.RedisConstants.LOGIN_EMPLOYEE_KEY;

/**
 * EmployeeController
 *
 * @author Gu
 * @date 2023/2/2
 * @Description:
 */
@RestController
@RequestMapping("/employ")
public class EmployeeController {
    @Resource
    private IEmployeeService employeeService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private IStoreService storeService;
    //账号密码登录
    @PostMapping("/login")
    public ResultDO login(@RequestBody LoginFormDTO loginForm, HttpSession session){
        //1.判空
        if (loginForm == null){
            return ResultDO.fail("请输入账号与密码");
        }
        //2.实现登录功能
        ResultDO data = employeeService.loginDO(loginForm, session);
        return data;
    }
    //手机验证码登录


    //发送验证码
    @GetMapping("/code")
    public ResultDO sendCode(@RequestParam String phone){
        //1.实现功能
    ResultDO resultDO = employeeService.sendCode(phone);
    return resultDO;
    }
    //验证码登录
    @PostMapping("/codeLogin")
    public ResultDO codeLogin(@RequestBody PhoneLoginDTO loginForm, HttpSession session){
        //1.判空
        if (loginForm == null){
            return ResultDO.fail("请输入手机号与验证码");
        }
        //2.实现登录功能
        ResultDO data = employeeService.codeLoginDo(loginForm, session);
        return data;
    }

    @DeleteMapping("/logout")
    public ResultDO logout(HttpServletRequest request){
        //清除redis token
        String token = request.getHeader("Authorization");
        stringRedisTemplate.expire(LOGIN_EMPLOYEE_KEY + token, 0L, TimeUnit.SECONDS);
        return ResultDO.ok();
    }

    @GetMapping("/getMsg")
    public ResultDO getMsg(){
        EmployeeDTO employee = EmployeeHolder.getEmployee();
        Store store = storeService.query().eq("account", employee.getStoreAccount()).one();
        employee.setStoreName(store.getName());
        //返回
        return ResultDO.ok(employee);
    }
    //分页查询(包含模糊搜索)
    @GetMapping(value={"/{currentPage}/{pageSize}/{name}", "/{currentPage}/{pageSize}"})
    public ResultDO listPage(@PathVariable(value = "currentPage", required = true) int currentPage,
                             @PathVariable(value = "pageSize", required = true) int pageSize,
                             @PathVariable(value = "name", required = false) String name){
        //1.判空
        if (currentPage <=0 || pageSize <= 0){
            return ResultDO.fail("错误");
        }
        //2.查询
        ResultDO resultDO = employeeService.pageList(currentPage, pageSize, name);
        //3.返回
        return resultDO;
    }

    //启用禁止员工接口
    @PutMapping("/isStart")
    public ResultDO isStart(@RequestBody StatusDTO employeeStatusDTO){
        //判断账号有没有权限
        if(!EmployeeHolder.getEmployee().getPermission()){
            return ResultDO.fail("抱歉，你的账号没有此权限");
        }
        //1.判空
        if(employeeStatusDTO == null){
            return ResultDO.fail("错误");
        }
        if(employeeStatusDTO.getIds() == null){
            return ResultDO.fail("错误");
        }
        //2.处理业务
        ResultDO resultDO = employeeService.isStart(employeeStatusDTO.getIds(), employeeStatusDTO.getIsStart());
        //3.返回结果
        return resultDO;
    }

    //修改员工信息
    @PutMapping("/changeEmployee")
    public ResultDO change(@RequestBody EmployeeDTO employeeDTO){
        //判断账号有没有权限
        if(!EmployeeHolder.getEmployee().getPermission()){
            return ResultDO.fail("抱歉，你的账号没有此权限");
        }
        //1.判空
        if(BeanUtil.isEmpty(employeeDTO)){
            return ResultDO.fail("出错了");
        }
        //2.处理业务
        ResultDO resultDO = employeeService.changeData(employeeDTO);
        //3.返回结果
        return resultDO;
    }
    //删除员工
    @DeleteMapping("/remove")
    public ResultDO remove(@RequestBody List<Long> ids){
        //判断账号有没有权限
        if(!EmployeeHolder.getEmployee().getPermission()){
            return ResultDO.fail("抱歉，你的账号没有此权限");
        }
        //1.判空
        if(ids == null){
            return ResultDO.fail("出错了");
        }
        //2.处理业务
        ResultDO resultDO = employeeService.removeEmployee(ids);
        //3.返回结果
        return resultDO;
    }
    //添加员工
    @PostMapping("/add")
    public ResultDO addEmployee(@RequestBody Employee employee){
        //判断账号有没有权限
        if(!EmployeeHolder.getEmployee().getPermission()){
            return ResultDO.fail("抱歉，你的账号没有此权限");
        }
        //1.判空
        if(employee == null){
            return ResultDO.fail("出错啦");
        }
        //2.业务处理
        ResultDO resultDO = employeeService.addEmployee(employee);
        //3.返回结果
        return resultDO;
    }

    //更改员工密码
    @PutMapping("/changePass")
    public ResultDO changePass(@RequestBody Employee employee){
        //1.判空
        if(employee == null){
            return ResultDO.fail("错误");
        }
        employee.setId(EmployeeHolder.getEmployee().getId());
        //2.处理业务
        //2.1将密码加密
        String s = SecureUtil.md5(employee.getPassword());
        employee.setPassword(s);
        //2.2判断是否修改图片
        if(StrUtil.isBlank(employee.getAvatar())){
            //如果不修改，将图片设置为空
            employee.setAvatar(null);
        }
        boolean isSuccess = employeeService.updateById(employee);
        //3.判断是否成功
        if(isSuccess){
            //如果成功,则将重新登录
            //返回
            return ResultDO.ok();
        }
        //返回
        return ResultDO.fail("修改失败，可能密码格式错误");
    }
    //上传员工头像
    @PostMapping("/uploadAvatar")
    public ResultDO uploadImage(@RequestParam("file") MultipartFile image)  {
        //1.获得文件名
        String filename = image.getOriginalFilename();
        //2.生成新的文件名
        //2.1获得后缀
        String suffix = StrUtil.subAfter(filename, ".", true);
        //2.2生成前缀
        String prefix = UUID.randomUUID().toString();
        //2.3生成目录
        File dir = new File(SystemConstants.IMAGE_UPLOAD_DIR, "/employee_avatar");
        //如果文件夹不存在,创建一个
        if (!dir.exists()) {
            dir.mkdirs();
        }
        //2.4生成新文件名
        String newName = StrUtil.format("/employee_avatar/{}.{}", prefix, suffix);

        //3.保存文件
        try {
            image.transferTo(new File(SystemConstants.IMAGE_UPLOAD_DIR, newName));
            return ResultDO.ok(newName);
        } catch (IOException e) {
            throw new RuntimeException("头像上传失败", e);
        }
    }
}
