package com.gu.xiang.controller;

import com.gu.xiang.dto.ResultDO;
import com.gu.xiang.service.ITOrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * OrderController
 *
 * @author Gu
 * @date 2023/2/11
 * @Description:
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Resource
    private ITOrderService orderService;

    //分页查询（带订单号模糊）
    @GetMapping(value={"/{currentPage}/{pageSize}/{orderNumber}", "/{currentPage}/{pageSize}"})
    public ResultDO listPage(@PathVariable(value = "currentPage", required = true) int currentPage,
                             @PathVariable(value = "pageSize", required = true) int pageSize,
                             @PathVariable(value = "orderNumber", required = false) Long orderNumber){
        //1.判空
        if (currentPage <=0 || pageSize <= 0){
            return ResultDO.fail("错误");
        }
        //2.查询
        ResultDO resultDO = orderService.pageList(currentPage, pageSize, orderNumber);
        //3.返回
        return resultDO;
    }

    //查询订单所对应的商品详情
    @GetMapping("/detail/{orderId}")
    public ResultDO detail(@PathVariable("orderId") Long orderId){
        //1.判空
        if(orderId == null){
            return ResultDO.fail("错误");
        }
        //2.查询
        ResultDO resultDO = orderService.queryDetail(orderId);
        //3.返回结果
        return resultDO;
    }
}
