package com.gu.xiang.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.gu.xiang.dto.DishesDTO;
import com.gu.xiang.dto.EmployeeDTO;
import com.gu.xiang.dto.StatusDTO;
import com.gu.xiang.dto.ResultDO;
import com.gu.xiang.entity.DishCategory;
import com.gu.xiang.entity.Dishes;
import com.gu.xiang.entity.Employee;
import com.gu.xiang.service.IDishCategoryService;
import com.gu.xiang.service.IDishesService;
import com.gu.xiang.utils.EmployeeHolder;
import com.gu.xiang.utils.SystemConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * DishesController
 *
 * @author Gu
 * @date 2023/2/9
 * @Description:
 */
@RestController
@RequestMapping("/dishes")
@Slf4j
public class DishesController {
    @Resource
    private IDishesService dishesService;
    @Resource
    private IDishCategoryService dishCategoryService;
    //分页查询（带模糊）
    @GetMapping(value={"/{currentPage}/{pageSize}/{name}", "/{currentPage}/{pageSize}"})
    public ResultDO listPage(@PathVariable(value = "currentPage", required = true) int currentPage,
                             @PathVariable(value = "pageSize", required = true) int pageSize,
                             @PathVariable(value = "name", required = false) String name){
        //1.判空
        if (currentPage <=0 || pageSize <= 0){
            return ResultDO.fail("错误");
        }
        //2.查询
        ResultDO resultDO = dishesService.pageList(currentPage, pageSize, name);
        //3.返回
        return resultDO;
    }

    //起售停售菜品
    @PutMapping("/isLaunch")
    public ResultDO isLaunch(@RequestBody StatusDTO statusDTO){
        //1.判空
        if(statusDTO == null){
            return ResultDO.fail("错误");
        }
        if(statusDTO.getIds() == null){
            return ResultDO.fail("错误");
        }
        //2.处理业务
        ResultDO resultDO = dishesService.isLaunch(statusDTO.getIds(), statusDTO.getIsStart());
        //3.返回结果
        return resultDO;
    }
    //删除菜品
    @DeleteMapping("/remove")
    public ResultDO remove(@RequestBody List<Long> ids){
        //判断账号有没有权限
        if(!EmployeeHolder.getEmployee().getPermission()){
            return ResultDO.fail("抱歉，你的账号没有此权限");
        }
        //1.判空
        if(ids == null){
            return ResultDO.fail("出错了");
        }
        //2.处理业务
        ResultDO resultDO = dishesService.removeDishes(ids);
        //3.返回结果
        return resultDO;
    }

    //获取菜品的所有种类
    @GetMapping("/allCategory")
    public ResultDO allCategory(){
        List<DishCategory> list = dishCategoryService.query().list();
        return ResultDO.ok(list);
    }

    //修改菜品信息
    @PutMapping("/changeDishes")
    public ResultDO change(@RequestBody Dishes dishes){
        //判断账号有没有权限
        if(!EmployeeHolder.getEmployee().getPermission()){
            return ResultDO.fail("抱歉，你的账号没有此权限");
        }
        //1.判空
        if(BeanUtil.isEmpty(dishes)){
            return ResultDO.fail("出错了");
        }
        //2.处理业务
        ResultDO resultDO = dishesService.changeData(dishes);
        //3.返回结果
        return resultDO;
    }

    //接收菜品照片
    @PostMapping("/uploadImage")
    public ResultDO uploadImage(@RequestParam("file") MultipartFile image)  {
        //判断账号有没有权限
        if(!EmployeeHolder.getEmployee().getPermission()){
            return ResultDO.fail("抱歉，你的账号没有此权限");
        }

        log.info("哈哈哈");
        //1.获得文件名
        String filename = image.getOriginalFilename();
        //2.生成新的文件名
        //2.1获得后缀
        String suffix = StrUtil.subAfter(filename, ".", true);
        System.out.println(suffix);
        //2.2生成前缀
        String prefix = UUID.randomUUID().toString();
        //2.3生成目录
        File dir = new File(SystemConstants.IMAGE_UPLOAD_DIR, "/food");
        //如果文件夹不存在,创建一个
        if (!dir.exists()) {
            dir.mkdirs();
        }
        //2.4生成新文件名
        String newName = StrUtil.format("/food/{}.{}", prefix, suffix);

        //3.保存文件
        try {
            image.transferTo(new File(SystemConstants.IMAGE_UPLOAD_DIR, newName));
            return ResultDO.ok(newName);
        } catch (IOException e) {
            throw new RuntimeException("文件上传失败", e);
        }
    }
    //添加菜品
    @PostMapping("/add")
    public ResultDO addDishes(@RequestBody Dishes dishes){
        //判断账号有没有权限
        if(!EmployeeHolder.getEmployee().getPermission()){
            return ResultDO.fail("抱歉，你的账号没有此权限");
        }
        //1.判空
        if(dishes == null){
            return ResultDO.fail("出错啦");
        }
        //2.业务处理
        ResultDO resultDO = dishesService.addDishes(dishes);
        //3.返回结果
        return resultDO;
    }
    //获得当前商铺所有菜品的名字和id
    @GetMapping("/getAll")
    public ResultDO getAll(){
        //1.获得商铺号
        //应该从EmployeeHolder里获取，这里直接测试
        List<Dishes> list = dishesService.query().eq("store_account","haochi").list();
        if(list == null){
            return ResultDO.fail("目前还没有菜品");
        }
        //2.返回结果
        return ResultDO.ok(list);
    }
}
