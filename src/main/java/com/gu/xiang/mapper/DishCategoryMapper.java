package com.gu.xiang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gu.xiang.entity.DishCategory;

/**
 * DishCategoryMapper
 *
 * @author Gu
 * @date 2023/2/9
 * @Description:
 */
public interface DishCategoryMapper extends BaseMapper<DishCategory> {
}
