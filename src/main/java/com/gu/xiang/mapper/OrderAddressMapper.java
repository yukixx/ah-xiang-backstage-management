package com.gu.xiang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gu.xiang.entity.OrderAddress;

/**
 * OrderAddressMapp
 *
 * @author Gu
 * @date 2023/2/11
 * @Description:
 */
public interface OrderAddressMapper extends BaseMapper<OrderAddress> {
}
