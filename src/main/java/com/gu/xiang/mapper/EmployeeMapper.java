package com.gu.xiang.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.gu.xiang.entity.Employee;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * EmployeeMapper
 *
 * @author Gu
 * @date 2023/2/2
 * @Description:
 */

public interface EmployeeMapper extends BaseMapper<Employee>{
    @Select("select account from employee ${ew.customSqlSegment}")
    String selectAccount(@Param(Constants.WRAPPER) Wrapper<String> queryWrapper);

}
