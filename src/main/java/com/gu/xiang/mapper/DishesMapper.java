package com.gu.xiang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gu.xiang.entity.Dishes;

/**
 * DishesMapper
 *
 * @author Gu
 * @date 2023/2/9
 * @Description:
 */
public interface DishesMapper extends BaseMapper<Dishes> {
}
