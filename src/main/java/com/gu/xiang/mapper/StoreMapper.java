package com.gu.xiang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gu.xiang.entity.Store;

/**
 * StoreMapper
 *
 * @author Gu
 * @date 2023/2/16
 * @Description:
 */
public interface StoreMapper extends BaseMapper<Store> {
}
