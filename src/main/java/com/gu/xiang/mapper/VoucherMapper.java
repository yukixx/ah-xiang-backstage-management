package com.gu.xiang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gu.xiang.entity.Voucher;

/**
 * VoucherMapper
 *
 * @author Gu
 * @date 2023/2/10
 * @Description:
 */
public interface VoucherMapper extends BaseMapper<Voucher> {
}
