package com.gu.xiang.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * UserDTO
 *
 * @author Gu
 * @date 2023/2/2
 * @Description:    可以返回给前端的员工信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDTO {
    private Long id;
    private String name;
    private String account;
    private String avatar;
    private Boolean permission;
    private String phone;
    private Boolean status;
    private String storeAccount;
    private String storeName;
}
