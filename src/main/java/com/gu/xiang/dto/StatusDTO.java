package com.gu.xiang.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * EmployeeStatusDTO
 *
 * @author Gu
 * @date 2023/2/6
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatusDTO {
    private List<Long> ids;
    private Boolean isStart;
}
