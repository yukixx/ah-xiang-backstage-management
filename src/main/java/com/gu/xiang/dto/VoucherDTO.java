package com.gu.xiang.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * VoucherDTO
 *
 * @author Gu
 * @date 2023/2/10
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoucherDTO {
    private Long id;
    private Integer categoryId;
    private String categoryName;
    private Long dishesId;
    private String dishesName;
    private Boolean status;
    private BigDecimal amount;
    private BigDecimal price;



}
