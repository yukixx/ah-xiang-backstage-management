package com.gu.xiang.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * DishesDTO
 *
 * @author Gu
 * @date 2023/2/9
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DishesDTO {
    private Long id;
    private String name;
    private String image;
    private BigDecimal price;
    private Boolean status;
    private Long categoryId;
    private String categoryName;
    private List<String> categoryList;
}
