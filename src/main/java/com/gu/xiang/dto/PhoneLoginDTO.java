package com.gu.xiang.dto;

import lombok.Data;

/**
 * PhoneLogin
 *
 * @author Gu
 * @date 2023/2/16
 * @Description:
 */
@Data
public class PhoneLoginDTO {
    private String phone;
    private String code;
}
