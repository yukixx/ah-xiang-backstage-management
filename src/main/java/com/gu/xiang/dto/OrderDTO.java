package com.gu.xiang.dto;

import com.gu.xiang.entity.OrderAddress;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * OrderDTO
 *
 * @author Gu
 * @date 2023/2/11
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {
    private Long id;
    private String userName;
    private String phone;
    private OrderAddress address;
    private Integer status;
    private BigDecimal trueAmount;
    private LocalDateTime createTime;

}
