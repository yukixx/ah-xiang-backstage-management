package com.gu.xiang.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * ResultDo
 *
 * @author Gu
 * @date 2023/2/2
 * @Description:
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ResultDO {
    //是否成功
    private Boolean isSuccess;
    //错误信息
    private String errorMsg;
    //返回的数据
    private Object data;
    //数据量
    private Long total;
    public static ResultDO ok(){
        return new ResultDO(true, null, null, null);
    }
    public static ResultDO ok(Object data){
        return new ResultDO(true, null, data, null);
    }
    public static ResultDO ok(List<?> data, Long total){
        return new ResultDO(true, null, data, total);
    }
    public static ResultDO fail(String errorMsg){
        return new ResultDO(false, errorMsg, null, null);
    }
}
