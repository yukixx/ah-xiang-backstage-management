package com.gu.xiang.dto;

import lombok.Data;

/**
 * LoginFormDTO
 *
 * @author Gu
 * @date 2023/2/2
 * @Description:
 */
@Data
public class LoginFormDTO {
    private String account;
    private String password;
    private String storeAccount;
}
