package com.gu.xiang.interceptor;

import cn.hutool.core.bean.BeanUtil;
import com.gu.xiang.dto.EmployeeDTO;
import com.gu.xiang.utils.EmployeeHolder;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.gu.xiang.utils.RedisConstants.LOGIN_EMPLOYEE_KEY;
import static com.gu.xiang.utils.RedisConstants.LOGIN_EMPLOYEE_TTL;

/**
 * TokenVerifyInterceptor
 *
 * @author Gu
 * @date 2023/2/4
 * @Description:
 */
public class TokenVerifyInterceptor implements HandlerInterceptor {
    private StringRedisTemplate stringRedisTemplate;
    //构造注入
    public TokenVerifyInterceptor(StringRedisTemplate stringRedisTemplate){
        this.stringRedisTemplate = stringRedisTemplate;
    }
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1.获得请求头token信息
        String token = request.getHeader("Authorization");
        if(token==null){//如果不存在，直接放行给login拦截器
            return true;
        }
        //2.存在，从redis中获取用户信息
        Map<Object, Object> map = stringRedisTemplate.opsForHash().entries(LOGIN_EMPLOYEE_KEY + token);
        if ( map == null || map.isEmpty()){//如果不存在，放行，交给登录拦截器
            return true;
        }
        //3.将用户信息放入ThreadLocal

        EmployeeDTO employeeDTO = BeanUtil.fillBeanWithMap(map, new EmployeeDTO(), false);
        EmployeeHolder.saveEmployee(employeeDTO);

        //4.刷新token
        stringRedisTemplate.expire(LOGIN_EMPLOYEE_KEY + token , LOGIN_EMPLOYEE_TTL, TimeUnit.MINUTES);

        //4.放行
        return true;
    }
}
