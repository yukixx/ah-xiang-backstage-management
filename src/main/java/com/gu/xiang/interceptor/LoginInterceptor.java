package com.gu.xiang.interceptor;

import com.gu.xiang.dto.EmployeeDTO;
import com.gu.xiang.utils.EmployeeHolder;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * LoginInterceptor
 *
 * @author Gu
 * @date 2023/2/2
 * @Description:    登录拦截器
 */
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1.判断登录员工信息是否存在
        EmployeeDTO employee = EmployeeHolder.getEmployee();
        if (employee == null){
            //不存在，响应401
            response.setStatus(401);// 401代表没有权限
            //拦截
            return false;
        }
        //2.如果存在，放行
        return true;
    }
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        EmployeeHolder.remove();
    }
}
