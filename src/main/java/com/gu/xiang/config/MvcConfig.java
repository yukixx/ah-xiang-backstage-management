package com.gu.xiang.config;

import com.baomidou.mybatisplus.autoconfigure.IdentifierGeneratorAutoConfiguration;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusLanguageDriverAutoConfiguration;
import com.gu.xiang.entity.Store;
import com.gu.xiang.interceptor.LoginInterceptor;
import com.gu.xiang.interceptor.MyCrosInterceptor;
import com.gu.xiang.interceptor.TokenVerifyInterceptor;
import com.iptool.interceptor.IpCountInterceptor;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.*;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * MvcConfig
 *
 * @author Gu
 * @date 2023/2/2
 * @Description:
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Resource
    private HttpServletResponse response;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private IpCountInterceptor ipCountInterceptor;
    //配置跨越
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                .allowedHeaders("*")
                .maxAge(3600);
    }
        
//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//        MappingJackson2HttpMessageConverter jackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
//        ObjectMapper objectMapper = new ObjectMapper();
//        SimpleModule simpleModule = new SimpleModule();
//        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
//        simpleModule.addSerializer(Long.TYPE,ToStringSerializer.instance);
//        objectMapper.registerModule(simpleModule);
//        jackson2HttpMessageConverter.setObjectMapper(objectMapper);
//        converters.add(0, jackson2HttpMessageConverter);
//    }

    //注册拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        //跨域拦截器
        registry.addInterceptor(new MyCrosInterceptor()).addPathPatterns("/**").order(0);
            //登录拦截器注册
            registry.addInterceptor(new LoginInterceptor())
                    .addPathPatterns("/**")
                    .excludePathPatterns("/employ/login", "/employ/code", "/employ/codeLogin").order(2);  //除了登录接口，全拦截
            //token认证拦截器注册
            registry.addInterceptor((new TokenVerifyInterceptor(stringRedisTemplate)))
                    .addPathPatterns("/**").order(1);
            //ip访问监视拦截器
            registry.addInterceptor(ipCountInterceptor).addPathPatterns("/**");
    }


}
