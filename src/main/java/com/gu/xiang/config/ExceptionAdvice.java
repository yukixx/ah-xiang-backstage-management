package com.gu.xiang.config;

import com.gu.xiang.dto.ResultDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * ExceptionAdvice
 *
 * @author Gu
 * @date 2023/2/12
 * @Description:
 */
@Slf4j
@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(RuntimeException.class)
    public ResultDO runtimeExceptionReturn(RuntimeException e){
        log.info("errorMsg", e);
        return ResultDO.fail("服务器异常");
    }
}
