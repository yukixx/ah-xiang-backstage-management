package com.gu.xiang;

import cn.hutool.crypto.SecureUtil;
import com.gu.xiang.dto.LoginFormDTO;
import com.gu.xiang.dto.ResultDO;
import com.gu.xiang.entity.TOrder;
import com.gu.xiang.service.IEmployeeService;
import com.gu.xiang.service.ITOrderService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@SpringBootTest
@Slf4j
class XiangApplicationTests {
    @Resource
    private IEmployeeService employeeService;
    @Resource
    private HttpSession httpSession;
    @Resource
    private ITOrderService orderService;
    @Test
    void contextLoads() {
        String md5 = SecureUtil.md5("12345");

    }
    @Test
    void logingTest(){
        LoginFormDTO loginFormDTO = new LoginFormDTO();
        loginFormDTO.setAccount("admin");
        loginFormDTO.setPassword("12345");
        ResultDO resultDO = employeeService.loginDO(loginFormDTO, httpSession);
        System.out.println(resultDO);
    }
    @Test
    void addOrderTest(){
        BigDecimal bigDecimal = new BigDecimal("20");
        /*Order order = new Order(null, 1L, 1397849739276890114L, 1, 1, 1,
                1, bigDecimal, 1L, LocalDateTime.now(), null);*/
        TOrder order = new TOrder();
        order.setUserId(1L);
        order.setGoodsId(1397849739276890114L);
        order.setNumber(1);
        order.setOrderCategory(1);
        order.setStatus(1);
        order.setPayType(1);
        order.setTrueAmount(bigDecimal);
        order.setAddressId(1L);
        order.setCreateTime(LocalDateTime.now());
        boolean save = orderService.save(order);
    }

}
