# 项目介绍
**阿祥后台管理项目是一个外卖管理系统，前后端分离。前端基于Vue框架，后端基于SpringBoot框架，主要实现了员工管理、菜品管理、代金券管理、订单管理等功能。**

# 项目展示

![image-20230313071657170](https://gitee.com/yukixx/ah-xiang-backstage-management/raw/master/src/main/resources/assets/image-20230313071657170.png)

![image-20230313071917603](https://gitee.com/yukixx/ah-xiang-backstage-management/raw/master/src/main/resources/assets/image-20230313071917603.png)

![image-20230313071954477](https://gitee.com/yukixx/ah-xiang-backstage-management/raw/master/src/main/resources/assets/image-20230313071954477.png)

![image-20230313072018304](https://gitee.com/yukixx/ah-xiang-backstage-management/raw/master/src/main/resources/assets/image-20230313072018304.png)

# 前端资源

[前端资源地址](https://gitee.com/yukixx/front-resource)

# 部署

**因为前端ajax请求地址为http://houtai/..，而项目的端口为8081，所以在nginx配置文件中做以下配置**



   upstream houtai{
		server localhost:8081;
	} 

server {

​	listen 80;

​	server_name localhost;

​	location /houtai/{
​			proxy_pass http://houtai/;
​	}

}

​	

