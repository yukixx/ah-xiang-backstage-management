/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.27-log : Database - db8
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db8` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db8`;

/*Table structure for table `dish_category` */

DROP TABLE IF EXISTS `dish_category`;

CREATE TABLE `dish_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) NOT NULL COMMENT '菜品名字',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_user` bigint(20) NOT NULL COMMENT '创建人',
  `update_user` bigint(20) NOT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `dish_category` */

insert  into `dish_category`(`id`,`name`,`create_time`,`update_time`,`create_user`,`update_user`) values (1,'热菜','2023-02-09 14:51:22','2023-02-09 14:52:38',2,2),(2,'凉菜','2023-02-09 14:52:47','2023-02-09 14:52:51',2,2),(3,'小吃','2023-02-09 14:53:17','2023-02-09 14:53:22',2,2),(4,'主食','2023-02-09 14:54:05','2023-02-09 14:54:10',2,2);

/*Table structure for table `dishes` */

DROP TABLE IF EXISTS `dishes`;

CREATE TABLE `dishes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '菜品名称',
  `category_id` bigint(20) NOT NULL COMMENT '菜品分类id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '菜品价格',
  `image` varchar(200) COLLATE utf8_bin NOT NULL COMMENT '图片',
  `store_account` varchar(32) COLLATE utf8_bin NOT NULL COMMENT '商铺号',
  `description` varchar(400) COLLATE utf8_bin DEFAULT NULL COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 停售 1 起售',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '顺序',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_user` bigint(20) NOT NULL COMMENT '创建人',
  `update_user` bigint(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1397849739276890123 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='菜品管理';

/*Data for the table `dishes` */

insert  into `dishes`(`id`,`name`,`category_id`,`price`,`image`,`store_account`,`description`,`status`,`sort`,`create_time`,`update_time`,`create_user`,`update_user`) values (1397849739276890114,'米饭',4,'8.00','/food/rice.jpg','haochi','神中神',1,0,'2021-05-27 09:38:43','2023-02-17 11:02:37',1,1),(1397849739276890115,'兰州拉面',4,'10.00','/food/lanzhou.jpg','haochi','神中神中神',1,0,'2023-02-09 16:25:30','2023-02-10 00:21:40',1,1),(1397849739276890121,'凉拌藕片',2,'20.00','/food/d8f281e3-1f66-4649-8ec6-a3e13318d2ab.jpg','haochi','凉拌藕片是一道经典的小吃，属于浙菜系。主要食材是莲藕，口味鲜香，爽口解腻，开胃下酒，还能起到养阴清热、润燥止渴、清心安神的作用。',1,0,'2023-02-10 14:58:47','2023-02-10 14:58:56',1,NULL),(1397849739276890122,'牛肉汤',3,'10.00','/food/4892d35c-eee0-4b99-82fc-c11fb1d2c73d.jpg','haochi','牛肉汤是一道汉族传统的美食，主料有牛大骨、 牛肉 。 牛肉汤选料讲究，取江淮一带的黄牛为原料，用牛骨头熬汤，煮牛肉时必须浸泡，除去血污，内脏清洗干净，方可下锅同煮。 还用自制的牛油，将炸制好淮椒（红干椒）做成红油。',1,0,'2023-02-12 15:54:30','2023-02-12 15:54:29',2,NULL);

/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) COLLATE utf8_bin NOT NULL COMMENT '员工姓名',
  `account` varchar(32) COLLATE utf8_bin NOT NULL COMMENT '用户名',
  `password` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '密码',
  `avatar` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '/employee_avatar/avatar.png' COMMENT '用户头像',
  `permission` tinyint(1) NOT NULL DEFAULT '0' COMMENT '权限(1为管理者, 0为员工)',
  `phone` varchar(11) COLLATE utf8_bin NOT NULL COMMENT '手机号',
  `sex` tinyint(1) NOT NULL DEFAULT '1' COMMENT '性别(男为1、女为0)',
  `id_number` varchar(18) COLLATE utf8_bin DEFAULT NULL COMMENT '身份证号',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 0:禁用，1:正常',
  `store_account` varchar(32) COLLATE utf8_bin NOT NULL COMMENT '商家号',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_user` bigint(20) NOT NULL COMMENT '创建人',
  `update_user` bigint(20) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=1622922095978188812 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='员工信息';

/*Data for the table `employee` */

insert  into `employee`(`id`,`name`,`account`,`password`,`avatar`,`permission`,`phone`,`sex`,`id_number`,`status`,`store_account`,`create_time`,`update_time`,`create_user`,`update_user`) values (2,'管理员','admin','e10adc3949ba59abbe56e057f20f883e','/employee_avatar/4.jpg',1,'13333333333',1,'111111111111111111',1,'haochi','2023-02-02 14:55:04','2023-02-17 16:13:49',2,2),(6,'管理员','admin','827ccb0eea8a706c4c34a16891f84e7b','/employee_avatar/avatar.png',1,'13333533333',1,'111111111111111111',1,'buhaochi','2023-02-02 14:55:04','2023-02-16 19:03:57',2,2),(23,'小wwddd','xaodadaadlddi','827ccb0eea8a706c4c34a16891f84e7b','/employee_avatar/avatar.png',0,'18888818888',1,'111111111111111111',1,'haochi','2023-02-02 14:55:04','2023-02-16 19:04:00',1,1),(24,'小wwddd','aoddaadlddi','827ccb0eea8a706c4c34a16891f84e7b','/employee_avatar/avatar.png',0,'18888888888',1,'111111111111111111',0,'haochi','2023-02-02 14:55:04','2023-02-17 11:13:44',1,1),(25,'小真','xiaozhen','827ccb0eea8a706c4c34a16891f84e7b','/employee_avatar/avatar.png',0,'18888828888',1,'111111111111111111',0,'haochi','2023-02-02 14:55:04','2023-02-17 10:56:01',1,1),(26,'小东西','xiaoxi','827ccb0eea8a706c4c34a16891f84e7b','/employee_avatar/avatar.png',0,'18888688888',1,'111111111111111111',1,'haochi','2023-02-02 14:55:04','2023-02-16 19:04:07',1,1),(1622922095978188805,'小李','xiaoli','e10adc3949ba59abbe56e057f20f883e','/employee_avatar/avatar.png',0,'13333433333',1,NULL,1,'haochi','2023-02-12 15:36:50','2023-02-16 19:04:11',2,NULL),(1622922095978188808,'小大','xiaoda','e10adc3949ba59abbe56e057f20f883e','/employee_avatar/avatar.png',0,'18888988888',1,NULL,1,'haochi','2023-02-12 16:32:35','2023-02-16 19:04:17',2,NULL),(1622922095978188809,'小孩子','xiaohaizi','87ac06bdb1d8374c76db2d404f2690ae','/employee_avatar/avatar.png',0,'18131516777',1,NULL,1,'haochi','2023-02-13 15:57:28','2023-02-13 15:57:27',2,NULL),(1622922095978188810,'老哥','laoge','5ae5bc6f25228abe5f3421bc17cf3259','/employee_avatar/avatar.png',0,'19999999999',1,NULL,1,'haochi','2023-02-13 19:24:01','2023-02-13 19:24:01',2,NULL),(1622922095978188811,'老姐','laojie','133ff1e10a8b244767ef734fb86f37fd','/employee_avatar/avatar.png',0,'18188888888',0,NULL,1,'haochi','2023-02-13 19:24:31','2023-02-16 19:04:22',2,NULL);

/*Table structure for table `order_address` */

DROP TABLE IF EXISTS `order_address`;

CREATE TABLE `order_address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `consignee` varchar(32) NOT NULL COMMENT '收货人',
  `phone` varchar(11) DEFAULT NULL COMMENT '收货人电话',
  `province` varchar(32) DEFAULT NULL COMMENT '省级名称',
  `city` varchar(32) DEFAULT NULL COMMENT '市级名称',
  `district` varchar(32) DEFAULT NULL COMMENT '区级名称',
  `detail` varchar(32) DEFAULT NULL COMMENT '详细地址',
  `is_default` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否默认 0否，1是',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `order_address` */

insert  into `order_address`(`id`,`user_id`,`consignee`,`phone`,`province`,`city`,`district`,`detail`,`is_default`,`create_time`,`update_time`) values (1,1,'小谷','13333333333','河南省','郑州市','','',1,'2023-02-11 14:51:38','2023-02-12 16:07:16');

/*Table structure for table `store` */

DROP TABLE IF EXISTS `store`;

CREATE TABLE `store` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) NOT NULL COMMENT '商家名字',
  `account` varchar(32) NOT NULL COMMENT '商家号',
  `password` varchar(64) NOT NULL COMMENT '登录密码',
  `address_id` bigint(20) NOT NULL COMMENT '地址',
  `phone` varchar(11) NOT NULL COMMENT '电话',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_accoutn` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `store` */

insert  into `store`(`id`,`name`,`account`,`password`,`address_id`,`phone`,`create_time`) values (1,'好吃','haochi','827ccb0eea8a706c4c34a16891f84e7b',1,'13838877913','2023-02-09 13:55:36'),(2,'不好吃','buhaochi','827ccb0eea8a706c4c34a16891f84e7b',2,'13838877913','2023-02-09 14:16:41');

/*Table structure for table `t_order` */

DROP TABLE IF EXISTS `t_order`;

CREATE TABLE `t_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键,也是订单号',
  `user_id` bigint(20) DEFAULT NULL COMMENT '下单用户id',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
  `number` int(11) DEFAULT '1' COMMENT '数量',
  `order_category` tinyint(1) DEFAULT NULL COMMENT '订单类型（1是菜品，2是代金券）',
  `phone` varchar(11) DEFAULT NULL COMMENT '电话',
  `store_account` varchar(32) NOT NULL COMMENT '商铺号',
  `status` tinyint(1) DEFAULT NULL COMMENT '订单状态（1待付款，2代派送，3已派送，4已完成，5已退款）',
  `pay_type` tinyint(1) DEFAULT NULL COMMENT '支付方式（1支付宝，2微信）',
  `true_amount` decimal(10,2) DEFAULT NULL COMMENT '实收金额',
  `address_id` bigint(20) DEFAULT NULL COMMENT '地址id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1624307433090842627 DEFAULT CHARSET=utf8;

/*Data for the table `t_order` */

insert  into `t_order`(`id`,`user_id`,`goods_id`,`number`,`order_category`,`phone`,`store_account`,`status`,`pay_type`,`true_amount`,`address_id`,`create_time`,`update_time`) values (1624307433090842626,1,1397849739276890114,1,1,'13333333333','haochi',1,1,'20.00',1,'2023-02-11 15:20:45','2023-02-12 16:06:22');

/*Table structure for table `voucher` */

DROP TABLE IF EXISTS `voucher`;

CREATE TABLE `voucher` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dishes_id` bigint(20) DEFAULT NULL COMMENT '对应菜品id（如果是通用，不填）',
  `name` varchar(32) NOT NULL COMMENT '代金券名字',
  `amount` decimal(10,2) NOT NULL COMMENT '代金券金额（保留两位小数）',
  `category_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '使用枚举Id（1代表通用，2代表限制菜品',
  `price` decimal(10,2) NOT NULL COMMENT '售价',
  `store_account` varchar(32) NOT NULL COMMENT '对应商铺号',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1起售0停售',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_user` bigint(20) NOT NULL COMMENT '创建人',
  `update_user` bigint(20) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `voucher` */

insert  into `voucher`(`id`,`dishes_id`,`name`,`amount`,`category_id`,`price`,`store_account`,`status`,`create_time`,`update_time`,`create_user`,`update_user`) values (1,1397849739276890121,'100元代金券','100.00',2,'70.00','haochi',1,'2023-02-10 15:26:36','2023-02-10 21:51:11',1,NULL),(2,NULL,'80元代金券','80.00',1,'70.00','haochi',1,'2023-02-10 18:20:02','2023-02-10 20:31:02',1,NULL),(3,NULL,'120元代金券','120.00',1,'100.00','haochi',1,'2023-02-12 15:55:15','2023-02-12 15:55:14',2,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
